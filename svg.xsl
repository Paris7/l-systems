<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns="http://www.w3.org/2000/svg">
   <xsl:output indent="yes" />

   <xsl:template match="/">
      <xsl:variable name="maxX"
         select="max(*/*/@x)" />
      <xsl:variable name="maxY"
         select="max(*/*/@y)" />
      <xsl:variable name="minX"
         select="min(*/*/@x)" />
      <xsl:variable name="minY"
         select="min(*/*/@y)" />

      <!-- Largeur et hauteur avec décalage -->
      <xsl:variable name="width"
         select="$maxX - $minX" />
      <xsl:variable name="height"
         select="$maxY - $minY" />

      <svg
         version="1.0"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         viewBox="0 0 {$width} {$height}">
         <xsl:apply-templates select="traceur">
            <xsl:with-param name="minX" select="$minX" />
            <xsl:with-param name="minY" select="$minY" />
            <xsl:with-param name="strokeWidth">
               <!-- On prend le plus grand côté qu'on divise
                    avec `d` une valeur arbitraire qui rend bien -->
               <xsl:variable name="d" select="1000" />

               <xsl:choose>
                  <xsl:when test="$width > $height">
                     <xsl:value-of select="$width div $d" />
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="$height div $d" />
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:with-param>
         </xsl:apply-templates>
      </svg>
   </xsl:template>

   <!-- Template pour le traceur -->
   <xsl:template match="traceur">
      <xsl:param
         name="minX" />
      <xsl:param
         name="minY" />
      <xsl:param
         name="strokeWidth" />

      <!-- Décalage -->
      <xsl:variable
         name="transX">
         <xsl:choose>
            <xsl:when test="0 > $minX">
               <xsl:value-of select="abs($minX)" />
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="0" />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable
         name="transY">
         <xsl:choose>
            <xsl:when test="0 > $minY">
               <xsl:value-of select="abs($minY)" />
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="0" />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <g
         stroke="black" stroke-width="{$strokeWidth}"
         transform="translate({$transX}, {$transY})">
         <xsl:call-template name="generatePath" />
      </g>
   </xsl:template>

   <!-- Tracés -->
   <xsl:template name="generatePath">
      <xsl:param name="idx"
         select="1" />
      <xsl:param name="x"
         select="0" />
      <xsl:param name="y"
         select="0" />

      <xsl:if test="count(*) >= $idx">
         <xsl:variable name="action"
            select="*[position() = $idx]" />

         <xsl:choose>
            <!-- Déplacement -->
            <xsl:when test="name($action) = 'MOVETO'">
               <xsl:call-template name="generatePath">
                  <xsl:with-param name="idx" select="$idx + 1" />
                  <xsl:with-param name="x" select="$action/@x" />
                  <xsl:with-param name="y" select="$action/@y" />
               </xsl:call-template>
            </xsl:when>

            <!-- Ecriture -->
            <xsl:when test="name($action) = 'LINETO'">
               <xsl:variable name="pos"
                  select="concat($x, ' ', $y)" />
               <xsl:variable name="nouvellePos"
                  select="concat($action/@x, ' ', $action/@y)" />

               <path
                  d="{concat('M', $pos, ' L', $nouvellePos)}" />

               <xsl:call-template
                  name="generatePath">
                  <xsl:with-param name="idx" select="$idx + 1" />
                  <xsl:with-param name="x" select="$action/@x" />
                  <xsl:with-param name="y" select="$action/@y" />
               </xsl:call-template>
            </xsl:when>
         </xsl:choose>
      </xsl:if>
   </xsl:template>
</xsl:transform>
