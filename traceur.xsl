<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:math="http://www.w3.org/2005/xpath-functions/math"
   xmlns:xs="http://www.w3.org/2001/XMLSchema">
   <xsl:output method="xml" indent="yes" />

   <!-- Déclaration des variables initiales -->
   <xsl:variable name="initial-x" select="0"
      as="xs:double" />
   <xsl:variable name="initial-y" select="0"
      as="xs:double" />
   <xsl:variable name="initial-angle" select="0"
      as="xs:double" />

   <!-- Séquences pour stocker les états -->
   <xsl:variable name="x-values" select="()"
      as="xs:double*" />
   <xsl:variable name="y-values" select="()"
      as="xs:double*" />
   <xsl:variable name="angle-values"
      as="xs:double*" select="()" />

   <xsl:template match="/tortue">
      <xsl:processing-instruction name="xml-model">href="traceur.xsd"</xsl:processing-instruction>

      <traceur>
         <!-- Appel du template pour traiter les commandes
              avec les valeurs initiales des paramètres -->
         <xsl:call-template name="process-commands">
            <xsl:with-param name="commands"
               select="*" />
            <xsl:with-param name="x"
               select="$initial-x" />
            <xsl:with-param name="y"
               select="$initial-y" />
            <xsl:with-param name="angle"
               select="$initial-angle" />
            <xsl:with-param name="x-values"
               select="$x-values" />
            <xsl:with-param name="y-values"
               select="$y-values" />
            <xsl:with-param name="angle-values"
               select="$angle-values" />
         </xsl:call-template>
      </traceur>
   </xsl:template>

   <!-- Template récursif pour traiter les commandes individuelles -->
   <xsl:template
      name="process-commands">
      <xsl:param
         name="commands" />
      <xsl:param
         name="x" />
      <xsl:param
         name="y" />
      <xsl:param
         name="angle" />
      <xsl:param
         name="x-values" />
      <xsl:param
         name="y-values" />
      <xsl:param
         name="angle-values" />

      <!-- Si il reste des commandes à traiter -->
      <xsl:if test="$commands">
         <!-- Sélection de la première commande à traiter -->
        <xsl:variable name="current-command"
            select="$commands[1]" />
        <xsl:choose>
            <!-- Si la commande est une ligne -->
            <xsl:when test="local-name($current-command) = 'line'">
               <!-- Calcul de la distance à parcourir
                    et conversion de l'angle en radians -->
                <xsl:variable name="distance"
                  select="number($current-command)" />
                <xsl:variable name="radians"
                  select="$angle * math:pi() div 180" />
                <xsl:variable name="new-x"
                  select="$x + $distance * math:cos($radians)" />
                <xsl:variable name="new-y"
                  select="$y + $distance * math:sin($radians)" />

               <!-- Création de l'élément LINETO
                    avec les nouvelles coordonnées calculées -->
                <LINETO
                  x="{format-number($new-x, '#.######')}"
                  y="{format-number($new-y, '#.######')}" />

               <!-- Appel récursif du template avec la mise à jour
                    des coordonnées et des commandes restantes -->
                <xsl:call-template
                  name="process-commands">
                  <xsl:with-param
                     name="commands" select="$commands[position() > 1]" />
                  <xsl:with-param
                     name="x" select="$new-x" />
                  <xsl:with-param
                     name="y" select="$new-y" />
                  <xsl:with-param
                     name="angle" select="$angle" />
                  <xsl:with-param
                     name="x-values" select="$x-values" />
                  <xsl:with-param
                     name="y-values" select="$y-values" />
                  <xsl:with-param
                     name="angle-values" select="$angle-values" />
               </xsl:call-template>
            </xsl:when>

            <!-- Si la commande est un turn -->
            <xsl:when test="local-name($current-command) = 'turn'">
               <!-- Calcul du nouvel angle avec modulo 360 pour rester
                    dans un cercle complet -->
                <xsl:variable name="new-angle"
                  select="($angle + number($current-command)) mod 360" />

               <!-- Appel récursif du template sans changer de
                    position mais en mettant à jour l'angle -->
                <xsl:call-template
                  name="process-commands">
                  <xsl:with-param name="commands"
                     select="$commands[position() > 1]" />
                  <xsl:with-param name="x"
                     select="$x" />
                  <xsl:with-param name="y"
                     select="$y" />
                  <xsl:with-param name="angle"
                     select="$new-angle" />
                  <xsl:with-param name="x-values"
                     select="$x-values" />
                  <xsl:with-param name="y-values"
                     select="$y-values" />
                  <xsl:with-param name="angle-values"
                     select="$angle-values" />
               </xsl:call-template>
            </xsl:when>

            <!-- Si la commande est un MOVE -->
            <xsl:when test="local-name($current-command) = 'move'">
               <!-- Calcul de la distance à parcourir et conversion
                    de l'angle en radians -->
                 <xsl:variable name="distance"
                  select="number($current-command)" />
                 <xsl:variable name="radians"
                  select="$angle * math:pi() div 180" />
                 <xsl:variable name="new-x"
                  select="$x + $distance * math:cos($radians)" />
                 <xsl:variable name="new-y"
                  select="$y + $distance * math:sin($radians)" />

               <!-- Création de l'élément MOVETO avec
                    les nouvelles coordonnées calculées -->
                 <MOVETO
                  x="{format-number($new-x, '#.######')}"
                  y="{format-number($new-y, '#.######')}" />

               <!-- Appel récursif du template avec la mise à jour
                    des coordonnées et des commandes restantes -->
               <xsl:call-template
                  name="process-commands">
                  <xsl:with-param name="commands"
                     select="$commands[position() > 1]" />
                  <xsl:with-param name="x"
                     select="$new-x" />
                  <xsl:with-param name="y"
                     select="$new-y" />
                  <xsl:with-param name="angle"
                     select="$angle" />
                  <xsl:with-param name="x-values"
                     select="$x-values" />
                  <xsl:with-param name="y-values"
                     select="$y-values" />
                  <xsl:with-param name="angle-values"
                     select="$angle-values" />
               </xsl:call-template>
            </xsl:when>

            <!-- Si la commande est un store -->
            <xsl:when test="local-name($current-command) = 'store'">
               <xsl:variable name="new-x-values"
                  select="($x-values, $x)" />
               <xsl:variable name="new-y-values"
                  select="($y-values, $y)" />
               <xsl:variable name="new-angle-values"
                  select="($angle-values, $angle)" />

               <!-- Appel récursif -->
               <xsl:call-template name="process-commands">
                  <xsl:with-param name="commands"
                     select="$commands[position() > 1]" />
                  <xsl:with-param name="x"
                     select="$x" />
                  <xsl:with-param name="y"
                     select="$y" />
                  <xsl:with-param name="angle"
                     select="$angle" />
                  <xsl:with-param name="x-values"
                     select="$new-x-values" />
                  <xsl:with-param name="y-values"
                     select="$new-y-values" />
                  <xsl:with-param name="angle-values"
                     select="$new-angle-values" />
               </xsl:call-template>
            </xsl:when>
            <!-- Si la commande est un restore -->
            <xsl:when test="local-name($current-command) = 'restore' and count($x-values) > 0">
               <xsl:variable name="last-x"
                  select="$x-values[last()]" />
               <xsl:variable name="last-y"
                  select="$y-values[last()]" />
               <xsl:variable name="last-angle"
                  select="$angle-values[last()]" />

               <!-- Création de l'élément MOVETO -->
               <MOVETO
                  x="{format-number($last-x, '#.######')}"
                  y="{format-number($last-y, '#.######')}" />

               <!-- Appel récursif -->
               <xsl:call-template
                  name="process-commands">
                  <xsl:with-param name="commands"
                     select="$commands[position() > 1]" />
                  <xsl:with-param name="x"
                     select="$last-x" />
                  <xsl:with-param name="y"
                     select="$last-y" />
                  <xsl:with-param name="angle"
                     select="$last-angle" />
                  <xsl:with-param name="x-values"
                     select="subsequence($x-values, 1, count($x-values) - 1)" />
                  <xsl:with-param name="y-values"
                     select="subsequence($y-values, 1, count($y-values) - 1)" />
                  <xsl:with-param name="angle-values"
                     select="subsequence($angle-values, 1, count($angle-values) - 1)" />
               </xsl:call-template>
            </xsl:when>
         </xsl:choose>
      </xsl:if>
   </xsl:template>
</xsl:transform>
