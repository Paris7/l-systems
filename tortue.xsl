<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output indent="yes" />

   <xsl:param name="nom" />
   <xsl:param name="n" />

   <!-- On récupère le Lsystem qui nous intéresse -->
   <xsl:template match="/">
      <xsl:processing-instruction name="xml-model">href="tortue.xsd"</xsl:processing-instruction>

      <xsl:apply-templates
         select="lsystems/lsystem[name=$nom]" />
   </xsl:template>

   <!-- Appel de la substitution -->
   <xsl:template match="lsystem">
      <tortue>
         <xsl:call-template name="substitution">
            <xsl:with-param name="actuel" select="axiom" />
            <xsl:with-param name="n" select="$n" />
         </xsl:call-template>
      </tortue>
   </xsl:template>

   <!-- Substitution -->
   <xsl:template name="substitution">
      <xsl:param name="actuel" />
      <xsl:param name="n" />

      <!-- Si on doit encore substituer -->
      <xsl:choose>
         <xsl:when test="$n > 0">
            <!-- Applique la substitution -->
            <xsl:variable name="data">
               <xsl:call-template name="Application">
                  <xsl:with-param name="data" select="$actuel" />
               </xsl:call-template>
            </xsl:variable>

            <!-- Appel récursif -->
            <xsl:call-template
               name="substitution">
               <xsl:with-param name="actuel" select="$data" />
               <xsl:with-param name="n" select="$n - 1" />
            </xsl:call-template>
         </xsl:when>

         <xsl:otherwise>
            <!-- Interprétation -->
            <xsl:call-template name="Interpretation">
               <xsl:with-param name="data" select="$actuel" />
            </xsl:call-template>

            <!-- DEBUG : Chaîne pas interprétée -->
            <!-- <xsl:value-of select="$actuel" /> -->
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Applique la substitution -->
   <xsl:template name="Application">
      <xsl:param name="data" />

      <xsl:variable name="substitution" select="substitutions/substitution" />

      <xsl:analyze-string
         select="$data" regex=".">
         <xsl:matching-substring>
            <xsl:variable name="sub" select="$substitution[@member = current()]" />

            <xsl:choose>
               <!-- Si pas de substitution trouvé, alors on renvoie le caractère
                    tel qu'il est -->
               <xsl:when test="not($sub)">
                  <xsl:value-of select="current()" />
               </xsl:when>

               <!-- Sinon renvois sa substitution -->
               <xsl:otherwise>
                  <xsl:value-of select="$sub" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:matching-substring>
      </xsl:analyze-string>
   </xsl:template>

   <!-- Interprète la séquence -->
   <xsl:template name="Interpretation">
      <xsl:param name="data" />

      <xsl:variable name="interpretation"
         select="interpretations/interpretation" />

      <xsl:analyze-string select="$data" regex=".">
         <xsl:matching-substring>
            <xsl:variable name="action"
               select="$interpretation[@member = current()]" />
            <xsl:choose>
               <!-- Ecrire -->
               <!-- TODO: Si plusieurs <line /> se suivent on pourrait les additionner -->
               <xsl:when test="starts-with($action, 'LINE')">
                  <line>
                     <xsl:value-of select="substring-after($action, ' ')" />
                  </line>
               </xsl:when>

               <!-- Déplacer -->
               <!-- TODO: Si plusieurs <move /> se suivent on pourrait les additionner -->
               <xsl:when test="starts-with($action, 'MOVE')">
                  <move>
                     <xsl:value-of select="substring-after($action, ' ')" />
                  </move>
               </xsl:when>

               <!-- Tourner -->
               <!-- TODO: Si plusieurs <turn /> se suivent on pourrait les additionner -->
               <xsl:when test="starts-with($action, 'TURN')">
                  <turn>
                     <xsl:value-of select="substring-after($action, ' ')" />
                  </turn>
               </xsl:when>

               <!-- Store -->
               <xsl:when test="current() = '['">
                  <store />
               </xsl:when>

               <!-- Restore -->
               <xsl:when test="current() = ']'">
                  <restore />
               </xsl:when>

               <!-- DEBUG : Affichage des actions non-gérés -->
               <!-- <xsl:otherwise>
                  <action>
                     <xsl:value-of select="$interpretation[@member = current()]" />
                  </action>
               </xsl:otherwise> -->
            </xsl:choose>
         </xsl:matching-substring>
      </xsl:analyze-string>
   </xsl:template>
</xsl:transform>
