CONVERTER = python converter.py
VALIDATE  = xmllint --schema
SAXON     = java -jar $(firstword $(wildcard saxon*.jar))
RM        = rm
ECHO      = echo -e
CURL      = curl --location
UNZIP     = unzip

SYSTEMS = l-systems
TORTUE  = tortue
TRACEUR = traceur
SVG     = svg

EXPORT = image

all: generation tortue traceur svg

# Generation du fichier XML des L-systèmes
generation:
	$(CONVERTER) $(SYSTEMS).csv -o $(SYSTEMS).xml
ifeq ($(NOCHECK),)
	$(VALIDATE) $(SYSTEMS).xsd $(SYSTEMS).xml 1>/dev/null
endif

# Génération du fichier XML de la tortue + vérification
tortue:
ifeq ($(and $(nom),$(n)),)
	@$(ECHO) "Il manque des arguments, par exemple :\n" \
           " make $(MAKECMDGOALS) nom=snow n=3"
	@false
endif
	$(SAXON) -s:$(SYSTEMS).xml -xsl:$(TORTUE).xsl -o:$(TORTUE).xml nom=$(nom) n=$(n)
ifeq ($(NOCHECK),)
	$(VALIDATE) $(TORTUE).xsd $(TORTUE).xml 1>/dev/null
endif

# Génération du fichier XML du traceur + vérification
traceur:
	$(SAXON) -s:$(TORTUE).xml -xsl:$(TRACEUR).xsl -o:$(TRACEUR).xml
ifeq ($(NOCHECK),)
	$(VALIDATE) $(TRACEUR).xsd $(TRACEUR).xml 1>/dev/null
endif

# Génération du fichier SVG
svg:
	$(SAXON) -s:$(TRACEUR).xml -xsl:$(SVG).xsl -o:$(EXPORT).svg

# Supprime les fichiers générés
clean:
	-$(RM) $(SYSTEMS).xml $(TORTUE).xml $(TRACEUR).xml $(EXPORT).svg

# Télécharge Saxon
SAXON_VER = 10.3
saxon:
	$(eval _version = $(subst .,-,$(SAXON_VER)))
	$(eval _major   = $(firstword $(subst ., ,$(SAXON_VER))))
	$(eval _zipfile = saxon.zip)
	$(CURL) https://github.com/Saxonica/Saxon-HE/raw/main/$(_major)/Java/SaxonHE$(_version)J.zip \
	  > $(_zipfile)
	$(UNZIP) -j $(_zipfile) saxon-he-$(SAXON_VER).jar
	$(RM) $(_zipfile)
