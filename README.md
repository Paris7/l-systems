# L-Systèmes

> Pré-requis : [Python 3](https://www.python.org/) et [Saxon](https://github.com/Saxonica/Saxon-HE)

## Usage

Le convertisseur est [converter.py](./converter.py).

```sh
$ python converter.py l-systems.csv
```

Autrement, pour aider à l'utilisation, il est possible d'utiliser le Makefile.

```sh
$ make nom=snow n=3 # génère une image.svg de "snow" avec 3 itérations
```

Il est également possible de le faire étape par étape

```sh
$ make generation          # génère le fichier XML des L-systèmes
$ make tortue nom=snow n=3 # génère tortue.xml de "snow" avec 3 itérations
$ make traceur             # génère traceur.xml
$ make svg                 # génère image.svg
```

Les fichiers XML des L-systèmes, de la tortue et du traceur sont vérifié
avec le fichier XSD lors de la génération.

## Rapport

<!-- TODO: Push le PDF uniquement quand il est finalisé. -->

Le rapport se trouve dans le dossier [report](./report/).
